import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Icon from '@material-ui/core/Icon';
import { NavLink } from 'react-router-dom'; 
import ImageAvatars from '../../../Components/ImageAvatars';
import SideMenuData from '../../../Data/Side-menu.json';

const useStyles = makeStyles((theme) => ({
    header: {
      height: theme.spacing(18),
      position: 'relative',
      '& > img': {
        height: theme.spacing(18),
        display: 'block',
        width: '100%',
        objectFit: 'fill'
      },
      '& .after': {
        position: 'absolute', 
        bottom: 0, 
        left:0,
        right:0,
        top: 0,
        background: 'rgba(0, 0, 0, 0.52)',
        color: '#f1f1f1', 
        transition: '.5s ease',
        opacity: 1,
        width: '100%',
        '& > div': {
            position: 'absolute',
            bottom: 0,
        }
      }
    },
    menuList: {
        display: 'flex',
        padding: '16px',
        fontSize: 13,
        '& .material-icons': {
            fontSize: 23,
        }
    }
}));

export default function SideMenu() {

    const classes = useStyles();

    return (

        <div>
            <div className={classes.header}>
                <img className="w-100" src="http://getwallpapers.com/wallpaper/full/e/c/9/200279.jpg" alt="gym" />
                <div className="after">
                    <div>
                        <div className="mb-10 ml-10">
                            <ImageAvatars size="large"/>
                            <div className="small">Mohammed sadik vk</div>
                            <div className="f-s-10">sadikvkm123@gmail.com</div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="mt-10">
                {SideMenuData.map((value, index) => (
                        <NavLink to={value.url} key={index} className={classes.menuList}>
                            <div><Icon>{value.icon}</Icon></div>
                            <div style={{marginTop: '3px'}} className="ml-10">{value.name}</div>
                        </NavLink>
                    ))}
            </div>
        </div>
    )
}