import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import { APP_LOGO } from '../../../Config'

const useStyles = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(6),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    }
}));

export default function AuthLayout(props) {

    const classes = useStyles();

    return (
        <div>
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <div className={classes.paper}>
                    <div><img style={{width: '11rem'}} src={APP_LOGO} alt="company" /></div>
                    <div className="f-s-13 mt-5">{props.title}</div>
                    <div className="mt-30 w-100">
                        {props.children}
                    </div>
                </div>
            </Container>
        </div>
    )
}