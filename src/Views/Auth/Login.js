import React, { useState } from 'react';
import AuthLayout from '../Layouts/Auth'
import AppTextField from '../../Components/AppTextField'
import { makeStyles } from '@material-ui/core/styles';
import ButtonLoader from '../../Components/ButtonLoader';
import HeaderHelmet from '../../Components/HeaderHelmet';
import { useHistory } from 'react-router-dom';
import useForceUpdate from 'use-force-update';
import SimpleReactValidator from 'simple-react-validator';
import { useDispatch } from 'react-redux'

//Actions
import { Authentication } from '../../Actions/Login';

const useStyles = makeStyles((theme) => ({
    container: {
      marginTop: theme.spacing(4),
    },
}));

export default function Login() {

    const classes = useStyles();
    const History = useHistory();
    const dispatch = useDispatch()
    const forceUpdate = useForceUpdate();

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState({errorResponse: ''});
    const [buttonLoader, setButtonLoader] = React.useState(false);

    const [validator] = React.useState(new SimpleReactValidator());

    const submitHandler = (e) => {
        e.preventDefault();
        if (validator.allValid()) {
            setButtonLoader(true);
            dispatch(Authentication({
                email: email,
                password: password
            }))
            .then(response => {

                let res = response.data;

                if(res.status === true) {
                    History.push('/dashboard')
                }else {
                    setButtonLoader(false);
                    setError({errorResponse: res.message})
                }
            })
        }else {
            validator.showMessages();
            forceUpdate();
        }
    }

    return (
        <div>
            <HeaderHelmet title="Login">
                <style>{'#login-button .MuiButton-containedSecondary{padding: 6px 80px !important;font-size: 0.825rem !important;}'}</style>
            </HeaderHelmet>
            <AuthLayout title="Sign in to your account">
                <div className="text-center error">{error.errorResponse}</div>
                <div className={classes.container}>
                    <div className="">
                        <AppTextField
                            fullWidth={true}
                            placeholder="Email"
                            type="email"
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                        />
                        {validator.message('Email', email, 'required')}
                    </div>
                    <div className="mt-30">
                        <AppTextField
                            fullWidth={true}
                            placeholder="Password"
                            type="password"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                        />
                        {validator.message('Password', password, 'required')}
                    </div>
                    <div className="mt-30 text-center" id="login-button">
                        <ButtonLoader onClick={submitHandler} buttonLoader={buttonLoader} fullWidth={false} color="secondary" size="medium" label="Sign In"></ButtonLoader>
                        <div className="mt-20">
                            Forgot password?
                        </div>
                        {/* <div className="mt-10">
                            Don't have an account? <span className="f-bold">Register</span>
                        </div> */}
                    </div>
                    <div className="divider mt-30"></div>
                    <div className="small mt-40 text-secondary">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    </div>
                </div>
            </AuthLayout>
        </div>
    )
}