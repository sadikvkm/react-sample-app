import React from 'react';
import ImageAvatars from '../../../../Components/ImageAvatars';
import DateFormat from '../../../../Components/DateFormat';
import { RESOURCE_URL } from '../../../../Config'

export default function ListView(props) {

    const value = props.value;

    return (
        <div>
            <div className="p-10 d-flex">
                <ImageAvatars image={RESOURCE_URL + '/profile/' +value.profile} size="large"/>
                <div className="mt-10 ml-10">
                    <div className="f-w-600 f-s-14">{value.first_name}</div>
                    <div className="f-s-12">{value.mobile}</div>
                    <div className="f-s-12">Join <DateFormat dateOnly={true} date={value.created_at} /></div>
                </div>
            </div>
            <div className="divider"></div>
        </div>
    )
}