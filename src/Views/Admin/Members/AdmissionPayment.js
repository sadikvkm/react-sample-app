import React from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import Pages from '../../../Components/Page';
import ButtonLoader from '../../../Components/ButtonLoader';
import AppTextField from '../../../Components/AppTextField';
import AppSelectBox from '../../../Components/AppSelectBox';
import { admissionTypes } from '../../../Actions/AdmissionTypes';
import { makePayment } from '../../../Actions/Payments';
import { getApplicationSettings } from '../../../Actions/ApplicationSettings';
import { membershipPlanList } from '../../../Actions/MembershipPlan';
import SimpleReactValidator from 'simple-react-validator';
import useForceUpdate from 'use-force-update';

export default function AdmissionPayment(props) {

    const History = useHistory();
    const dispatch = useDispatch();
    const forceUpdate = useForceUpdate();

    const [validator] = React.useState(new SimpleReactValidator());
    const [buttonLoader, setButtonLoader] = React.useState(false);
    const [admissionType, setAdmissionType] = React.useState('');
    const [membershipPlan, setMembershipPlan] = React.useState('');
    const [admissionDetails, setAdmissionDetails] = React.useState('');
    const [membershipPlanDetails, setMembershipPlanDetails] = React.useState('');
    const [admissionTypeAmount, setAdmissionTypeAmount] = React.useState(0.00)
    const [membershipPlanAmount, setMembershipPlanAmount] = React.useState(0.00)
    const [totalTax, setTotalText] = React.useState(0)
    const [subTotal, setSubTotal] = React.useState(0.00);
    const [totalAmount, setTotalAmount] = React.useState(0.00);
    const [paidAmount, setPaidAmount] = React.useState(0.00);
    const memberId = props.match.params.memberId;

    React.useEffect(() => {
        window.scrollTo(0, 0)
        dispatch(admissionTypes())
        dispatch(membershipPlanList())
        dispatch(getApplicationSettings())

    }, [dispatch]);

    const { allAdmissionTypes, allMembershipPlans, admissionAmount } = useSelector(state => ({
        allAdmissionTypes: state.admissionType.list,
        allMembershipPlans: state.membershipPlan.list,
        admissionAmount: state.application.admissionAmount
    }));

    const submitHandler = (e) => {
        e.preventDefault();

        if (validator.allValid()) {
            // setButtonLoader(true);
            dispatch(makePayment({
                memberId: memberId,
                admission_type: admissionType,
                membership_plan: membershipPlan,
                paid_amount: paidAmount,
                total: totalAmount
            }))
            .then(res => {
                History.push('/members/create/success/' + res.result.register_id);
            })
        }else {
            window.scrollTo(0, 0)
            validator.showMessages();
            forceUpdate();
        }
    }

    const handleAdmissionType = async(e) => {

        let admissionId = e.target.value;

        setAdmissionType(admissionId);

        if(admissionId) {
            let details = allAdmissionTypes.find(i => i.id = admissionId);
            setAdmissionDetails(details)
            setAdmissionTypeAmount({amount: details.amount, tax: details.tax})
            calculate(details)            
        }else {
            setAdmissionDetails('')
            calculate(admissionTypeAmount, '-')
        }
    }

    const handleMembershipPlan = async(e) => {

        let membershipId = e.target.value;

        setMembershipPlan(membershipId);

        if(membershipId) {
            let details = allMembershipPlans.find(i => i.id = membershipId);
            setMembershipPlanDetails(details)
            setMembershipPlanAmount({amount: details.amount, tax: details.tax})
            calculate(details)
        }else {
            setMembershipPlanDetails('')
            calculate(membershipPlanAmount, '-')
        }
    }

    const calculate = (details, operation = '+') => {

        if(operation === '+') {
            setSubTotal(parseInt(subTotal) + parseInt(details.amount))
            setTotalText(parseInt(totalTax) + parseInt(details.tax))
            setTotalAmount(parseFloat(totalAmount) + parseFloat(details.amount))
        }else {
            setSubTotal(subTotal - details.amount)
            setTotalText(totalTax - details.tax)
            setTotalAmount(parseFloat(totalAmount) - parseFloat(details.amount))
        }
    }

    const getAdmissionTypeBlock = () => {
        
        if(admissionDetails) {
            return (
                <tr>
                    <td><div>Admission type: {admissionDetails.admission_type}</div></td>
                    <td className="text-right">{Number(admissionDetails.amount).toFixed(2)}</td>
                </tr>
            )
        }
        return;
    }

    const getMembershipPlanBlock = () => {

        if(membershipPlanDetails) {
            return (
                <tr>
                    <td><div>{membershipPlanDetails.membership_plan}</div></td>
                    <td className="text-right">{Number(membershipPlanDetails.amount).toFixed(2)}</td>
                </tr>
            )
        }
        return;
    }

    const getTotalBlock = () => {

        if(admissionDetails || membershipPlanDetails) {
            return (
                <tr>
                    <td></td>
                    <td className="text-right">
                        <div className="divider mt-10 mb-10"></div>
                        <table className="w-100">
                            <tbody>
                                <tr>
                                    <td className="text-left f-w-600">Sub total</td>
                                    <td className="text-right">{Number(subTotal + admissionAmount).toFixed(2)}</td>
                                </tr>
                                {/* <tr>
                                    <td className="text-left f-w-600">Tax</td>
                                    <td className="text-right">{Number(totalTax).toFixed(2)}</td>
                                </tr> */}
                                <tr>
                                    <td></td>
                                    <td><div className="divider mt-10 mb-10"></div></td>
                                </tr>
                                <tr>
                                    <td className="text-left f-w-600">Total</td>
                                    <td className="text-right">{Number(totalAmount + admissionAmount).toFixed(2)}</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            )
        }
        return;
    }
    
    return (
        <Pages container="container">
            <div className="mt-20 mb-20">
                <form className="form mt-30">
                    <div className="card-title">Payment</div>
                    <div className="form-group">
                        <AppSelectBox value={admissionType} onChange={handleAdmissionType} required fullWidth label="Admission types">
                            {allAdmissionTypes.map((value, index) => <MenuItem key={index} value={value.id}>{value.admission_type}</MenuItem>)}
                        </AppSelectBox>
                        {validator.message('admission type', admissionType, 'required')}
                    </div> 
                    <div className="form-group">
                        <AppSelectBox value={membershipPlan} onChange={handleMembershipPlan} required fullWidth label="Admission plan">
                            {allMembershipPlans.map((value, index) => <MenuItem key={index} value={value.id}>{value.membership_plan}</MenuItem>)}
                        </AppSelectBox>
                        {/* {validator.message('membership plan', membershipPlan, 'required')} */}
                    </div> 
                    <div className="small mt-30">
                        <table className="w-100">
                            <tbody>
                                <tr>
                                    <td><div>Admission amount</div></td>
                                    <td className="text-right">{Number(admissionAmount).toFixed(2)}</td>
                                </tr>
                                {getAdmissionTypeBlock()}
                                {getMembershipPlanBlock()}
                                {getTotalBlock()}
                            </tbody>
                        </table>
                    </div>
                    <div className="mt-10 form-group">
                        <AppTextField value={paidAmount} onChange={e => setPaidAmount(e.target.value)} type="number" fullWidth label="Paid Amount" placeholder="Paid Amount"/>
                    </div>
                    <div className="mt-20">
                        <ButtonLoader onClick={submitHandler} buttonLoader={buttonLoader} fullWidth={true} size="medium" label="Submit"></ButtonLoader>
                    </div>
                </form>              
            </div> 
        </Pages>
    );
}