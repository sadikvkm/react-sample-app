import React from 'react';
import HeaderHelmet from '../../../Components/HeaderHelmet';
import AppCard from '../../../Components/AppCard';
import { makeStyles, useTheme  } from '@material-ui/core/styles';
import Pages from '../../../Components/Page';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import SwipeableViews from 'react-swipeable-views';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import PropTypes from 'prop-types';
import EventIcon from '@material-ui/icons/Event';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import PersonIcon from '@material-ui/icons/Person';
import OpacityIcon from '@material-ui/icons/Opacity';
import ContactPhoneIcon from '@material-ui/icons/ContactPhone';
import HomeWorkIcon from '@material-ui/icons/HomeWork';
import PersonPinCircleIcon from '@material-ui/icons/PersonPinCircle';
import AccessibilityIcon from '@material-ui/icons/Accessibility';
import Fab from '@material-ui/core/Fab';
import EditIcon from '@material-ui/icons/Edit';
// import FitnessCenterIcon from '@material-ui/icons/FitnessCenter';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import ComingSoon from '../../../Components/ComingSoon';

function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`full-width-tabpanel-${index}`}
        aria-labelledby={`full-width-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box p={3}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
}
  
TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};
  
function a11yProps(index) {
    return {
      id: `full-width-tab-${index}`,
      'aria-controls': `full-width-tabpanel-${index}`,
    };
}

const useStyles = makeStyles((theme) => ({ 
    userBasicDetails: {
        padding: 10,
        textAlign: 'center',
        '& img': {
            width: '25%',
            height: '25%',
            borderRadius: '4rem'
        },
        '& div': {
            color: '#fff',
            fontWeight: 600
        }

    },
    appCard: {
      marginTop: theme.spacing(1),
      '& .MuiPaper-rounded': {
        borderRadius: '14px 14px 0px 0px'
      }
    },
    tabSection: {

        '& .react-swipeable-view-container': {
            // height: '23.1rem !important',
        }
    }
}));

export default function View(props) {

    const classes = useStyles();

    const theme = useTheme();
    const [value, setValue] = React.useState(0);
    const [openComingSoon, setOpenComingSoon] = React.useState(false);

    React.useEffect(() => {
        window.scrollTo(0, 0)
    }, [])

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const handleChangeIndex = (index) => {
        setValue(index);
    };

    return (
        <div>
            <HeaderHelmet title="Dashboard">
                <style>{'.MuiFab-root {box-shadow: 0px 0px 0px -1px rgba(0,0,0,0.2), 0px 0px 0px 0px rgba(0,0,0,0.14), 0px 0px 1px 0px rgba(0,0,0,0.12)} #tab-section .MuiPaper-elevation4 {box-shadow: 0px 4px 5px 0px #0000002b !important;} #tab-section .MuiTabs-flexContainer .MuiTab-wrapper { font-size: 12px}'}</style>
            </HeaderHelmet>
            <Pages enableAutoDevice>
                <div style={{background: 'rgb(63, 81, 181)', display: 'flow-root', height: '32vh'}}>
                    <div className={classes.userBasicDetails}>
                        <img src="https://www.atlassian.com/dam/jcr:ba03a215-2f45-40f5-8540-b2015223c918/Max-R_Headshot%20(1).jpg" alt="user" />
                        <div>
                            <div>#745214</div>
                            <div>Mohammed sadik vk</div>
                        </div>
                    </div>
                    <div className={classes.appCard}>
                        <AppCard>
                            <div className="mt-10 d-flex text-center">
                                <div className="w-100">
                                    <div className="small text-secondary">Join date</div>
                                    <div className="small">20, Feb 2020</div>
                                </div>
                                <div className="w-100">
                                    <div className="small text-secondary">Mobile</div>
                                    <div className="small">+919633388448</div>
                                </div>
                            </div>

                            <div className="mt-30 d-flex text-center">
                                <div className="w-100">
                                    <Fab size="small">
                                        <EditIcon />
                                    </Fab>
                                </div>
                                {/* <div className="w-100">
                                    <Fab onClick={() => setOpenComingSoon(true)} size="small">
                                        <FitnessCenterIcon />
                                    </Fab>
                                </div> */}
                                <div className="w-100">
                                    <Fab size="small">
                                        <VisibilityOffIcon />
                                    </Fab>
                                </div>
                            </div>
                        </AppCard>
                        <div id="tab-section" className={classes.tabSection}>
                            <AppBar position="static" color="white">
                                <Tabs
                                    value={value}
                                    onChange={handleChange}
                                    indicatorColor="primary"
                                    textColor="primary"
                                    variant="fullWidth"
                                    aria-label="full width tabs example"
                                    >
                                        <Tab label="Overview" {...a11yProps(0)} />
                                        <Tab label="Fee history" {...a11yProps(1)} />
                                        <Tab label="Attendance" {...a11yProps(2)} />
                                </Tabs>
                            </AppBar>
                            <SwipeableViews
                                axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                                index={value}
                                onChangeIndex={handleChangeIndex}
                            >
                                <TabPanel value={value} index={0} dir={theme.direction}>
                                    <div>
                                        <div className="d-flex mt-10">
                                            <div><EventIcon /></div>
                                            <div className="ml-20">
                                                <div className="small text-secondary">Date of birth</div>
                                                <div className="small">28, Dec 1993</div>
                                            </div>
                                        </div>
                                        <div className="d-flex mt-10">
                                            <div><PersonIcon /></div>
                                            <div className="ml-20">
                                                <div className="small text-secondary">Gender</div>
                                                <div className="small">Male</div>
                                            </div>
                                        </div>
                                        <div className="d-flex mt-10">
                                            <div><OpacityIcon /></div>
                                            <div className="ml-20">
                                                <div className="small text-secondary">Blood Group</div>
                                                <div className="small">A+</div>
                                            </div>
                                        </div>
                                        <div className="d-flex mt-10">
                                            <div><MailOutlineIcon /></div>
                                            <div className="ml-20">
                                                <div className="small text-secondary">Email</div>
                                                <div className="small">sadikvkm123@gmail.com</div>
                                            </div>
                                        </div>
                                        <div className="d-flex mt-10">
                                            <div><ContactPhoneIcon /></div>
                                            <div className="ml-20">
                                                <div className="small text-secondary">Emergency contact name</div>
                                                <div className="small">Shajahan</div>
                                            </div>
                                        </div>
                                        <div className="d-flex mt-10">
                                            <div><ContactPhoneIcon /></div>
                                            <div className="ml-20">
                                                <div className="small text-secondary">Emergency contact number</div>
                                                <div className="small">+919633388445</div>
                                            </div>
                                        </div>
                                        <div className="d-flex mt-10">
                                            <div><HomeWorkIcon /></div>
                                            <div className="ml-20">
                                                <div className="small text-secondary">Current Address</div>
                                                <div className="small">Sample address from dev</div>
                                            </div>
                                        </div>
                                        <div className="d-flex mt-10">
                                            <div><HomeWorkIcon /></div>
                                            <div className="ml-20">
                                                <div className="small text-secondary">Permanent address</div>
                                                <div className="small">Sample address from dev</div>
                                            </div>
                                        </div>
                                        <div className="d-flex mt-10">
                                            <div><PersonPinCircleIcon /></div>
                                            <div className="ml-20">
                                                <div className="small text-secondary">Place</div>
                                                <div className="small">Valayamkulam</div>
                                            </div>
                                        </div>
                                        <div className="d-flex mt-10">
                                            <div><AccessibilityIcon /></div>
                                            <div className="ml-20">
                                                <div className="small text-secondary">Body details</div>
                                                <div className="small">Heigh 173, Weight 80Kg</div>
                                            </div>
                                        </div>
                                    </div>
                                </TabPanel>
                                <TabPanel value={value} index={1} dir={theme.direction}>
                                    <div>
                                        {['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'].map((value, index) => (
                                            <div key={index}>
                                                <div className="d-flex small">
                                                    <div className="w-100">
                                                        <div className="f-w-600">{value}</div>
                                                        <div>Due date: 20, May 2020</div>
                                                        <div className="text-warning">Pending</div>
                                                    </div>
                                                    <div className="text-right w-30">
                                                        500.00
                                                    </div>
                                                </div>
                                                <div className="divider mb-10 mt-10"></div>
                                            </div>
                                        ))}
                                    </div>
                                </TabPanel>
                                <TabPanel value={value} index={2} dir={theme.direction}>
                                    Item Three
                                </TabPanel>
                            </SwipeableViews>
                        </div>
                    </div>
                </div>
            </Pages>
            <ComingSoon open={openComingSoon} close={() => setOpenComingSoon(false)} />
        </div>
    )
}