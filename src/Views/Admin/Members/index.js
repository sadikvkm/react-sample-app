import List from './List';
import View from './View';
import New from './New';
import IdForm from './IdForm';
import SuccessNewMember from './SuccessNewMember';
import AdmissionPayment from './AdmissionPayment';

export {
    List,
    View,
    New,
    IdForm,
    AdmissionPayment,
    SuccessNewMember
}