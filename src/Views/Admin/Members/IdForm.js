import React, { useState } from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import SimpleReactValidator from 'simple-react-validator';
import useForceUpdate from 'use-force-update';

import Pages from '../../../Components/Page';
import ButtonLoader from '../../../Components/ButtonLoader';
import AppTextField from '../../../Components/AppTextField';
import AppSelectBox from '../../../Components/AppSelectBox';
import { IdTypes } from '../../../Data';
import DateFormat from '../../../Components/DateFormat';
import { getMemberDetails, updateMemberIdProof } from '../../../Actions/Members';
import { fileUpload } from '../../../Actions/FIleUpload';
import { RESOURCE_URL } from '../../../Config'

const useStyles = makeStyles((theme) => ({ 
    
    previewUploadImage: {
        padding: '20px',
        background: 'rgb(206, 206, 206)',
        borderRadius: 3,
        height: '5rem',
        width: 238
    },
    choosePhotoUpload: {
        marginTop: 17,
        '& button > span.MuiButton-label': {
            fontSize: '10px'
        }
    }
}));

export default function IdForm(props) {

    const classes = useStyles();
    const History = useHistory();
    const dispatch = useDispatch();
    const forceUpdate = useForceUpdate();

    const [validator] = React.useState(new SimpleReactValidator());
    const [userImage, setUserImage] = useState('');
    const [uploadIdProof, setUploadIdProof] = useState('');
    const [idType, setIdType] = useState('');
    const [idNumber, setIdNumber] = useState('');
    const [uploadMemberPhoto, setUploadMemberPhoto] = useState('');
    const [memberIdProof, setMemberIdProof] = useState('');
    const [buttonDisabled, setButtonDisabled] = useState(true);

    const [buttonLoader, setButtonLoader] = useState(false);
    const [buttonUploadLoader] = useState(false);
    const memberId = props.match.params.memberId;

    const { member } = useSelector(state => ({
        member: state.members.latestMember
    }))

    React.useEffect(() => {
        window.scrollTo(0, 0)

        if(! member) {
            dispatch(getMemberDetails(memberId))
            .then(response => {
                let memberProofDetails = response.result.memberProof;
                setIdType(memberProofDetails.id_type)
                setIdNumber(memberProofDetails.id_number)

                if(memberProofDetails.id_proof)
                    setUploadIdProof({
                        name: memberProofDetails.id_proof
                    });
                if(response.result.profile)    
                    setUserImage(RESOURCE_URL + '/profile/' + response.result.profile);
                
                if(memberProofDetails.id_type && memberProofDetails.id_number && memberProofDetails.id_proof && response.result.profile) {
                    setButtonDisabled(false)
                }
            })
        }

    }, [member, dispatch, memberId]);
    
    const submitHandler = (e) => {
        e.preventDefault();

        if (validator.allValid()) {

            setButtonLoader(true);
            dispatch(updateMemberIdProof(memberId, {
                profile: uploadMemberPhoto,
                id_type: idType,
                id_number: idNumber,
                id_proof: memberIdProof,
            }))
            .then(res => {
                setButtonLoader(false);
                if(res.status) {
                    History.push('/members/admission-payment/' + memberId);
                }
            })

        }else {
            window.scrollTo(0, 0)
            validator.showMessages();
            forceUpdate();
        }
    }

    const handleChangeFIleUpload = (e) => {

        let file = e.target.files[0];
        let reader = new FileReader();
        reader.readAsDataURL(file);

        reader.onloadend = function (e) {
            setUserImage(reader.result)
            let formData = new FormData();
            formData.append('file', file);
            dispatch(fileUpload(formData, 'profile'))
            .then(response => {
                setUploadMemberPhoto(response.file_path);
            })
        };
    }

    const handleChangeIdProofUpload = (e) => {

        let file = e.target.files[0];
        setUploadIdProof(file);
        let formData = new FormData();
        formData.append('file', file);
        dispatch(fileUpload(formData, 'id-proof'))
        .then(response => {
            setMemberIdProof(response.file_path);
        })
        
    }

    const submitButton = () => {

        let disabled = true;
        if(idType && idNumber && uploadMemberPhoto && memberIdProof) {
            disabled = false
        }

        return (
            <div className="mt-20">
                <ButtonLoader disabled={disabled ? buttonDisabled : disabled} onClick={submitHandler} buttonLoader={buttonLoader} fullWidth={true} size="small" label="Submit"></ButtonLoader>
            </div>
        )
    }
    
    return (
        <Pages container="container">
            <div className="mt-20 mb-20">
                <div className="card-title">Upload Image</div>
                
                <div className="d-flex mt-20">
                    
                    {userImage ? 
                        <img style={{width: '44%', objectFit: 'cover', height: '120px'}} src={userImage} alt="user" />
                    :
                        <div className={classes.previewUploadImage + ' small'}>
                            <div className="mt-30 text-center text-secondary">Member Photo</div>
                        </div>
                    }

                    <div className="ml-10 small w-100">
                        <div style={{lineHeight: '19px'}}>
                            <div className="f-bold">#{member.register_id}</div>
                            {member.first_name ? 
                                <div className="f-bold">{member.first_name + ' ' + member.middle_name + ' ' + member.last_name}</div>
                                : null
                            }
                            <div>Date of birth: <DateFormat dateOnly={true} date={member.dob} /></div>
                            <div>Join date: <DateFormat dateOnly={true} date={member.created_at} /></div>
                        </div>
                        <div className={classes.choosePhotoUpload} >
                            <ButtonLoader 
                                startIcon={<InsertDriveFileIcon />}
                                className={'text-left'}
                                onClick={() => document.getElementById("choose-user-image").click()} 
                                buttonLoader={buttonUploadLoader} 
                                fullWidth={false} 
                                size="small" 
                                color="default"
                                label="Choose photo *"
                            />
                            <input onChange={handleChangeFIleUpload} className="hidden" type="file" id="choose-user-image" />
                        </div>
                    </div>
                </div>
                {validator.message('user image', userImage, 'required')}


                <form className="form mt-30">
                    <div className="card-title">Identification Proof</div>
                    
                    <div className="form-group">
                        <AppSelectBox value={idType} onChange={e => setIdType(e.target.value)} required fullWidth label="ID Types">
                            {IdTypes().map((value, index) => <MenuItem key={index} value={value.id}>{value.name}</MenuItem>)}
                        </AppSelectBox>
                        {validator.message('id type', idType, 'required')}
                    </div>
                    <div className="form-group">
                        <AppTextField value={idNumber} onChange={e => setIdNumber(e.target.value)} required fullWidth label="ID number" placeholder="ID number"/>
                        {validator.message('id number', idNumber, 'required')}
                    </div>
                    <div className={classes.choosePhotoUpload}>
                        {uploadIdProof ? 
                        
                            <div className="d-flex small">
                                <div className="text-success"><CheckCircleOutlineIcon style={{fontSize: '15px'}} /></div>
                                <div className="ml-10">{uploadIdProof.name}</div>
                                <div onClick={() => document.getElementById("choose-user-id-proof").click()} className="ml-10"><u>Change</u></div>
                            </div>
                        
                        :
                            <div>
                                <ButtonLoader 
                                    startIcon={<CloudUploadIcon />}
                                    className={'text-left'}
                                    onClick={() => document.getElementById("choose-user-id-proof").click()} 
                                    buttonLoader={buttonUploadLoader} 
                                    fullWidth={false} 
                                    size="small" 
                                    color="default"
                                    label="Upload ID proof *"
                                />
                            </div>
                        }
                        <input onChange={handleChangeIdProofUpload} className="hidden" type="file" id="choose-user-id-proof" />
                        {validator.message('id proof', uploadIdProof, 'required')}
                    </div>
                    {submitButton()}
                </form>
                
            </div> 
        </Pages>
    );
}