import React from 'react';
import { NavLink } from 'react-router-dom';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import FloatingActionButtons from '../../../Components/FloatingActionButtons';
import { membersList } from '../../../Actions/Members';
import * as MemberComponent from './Components';
import Page from '../../../Components/Page';
import EmptyData from '../../../Components/EmptyData';

export default function List(props) {

    const history = useHistory();
    const dispatch = useDispatch();

    React.useEffect(() => {
        window.scrollTo(0, 0);
        dispatch(membersList());
    }, []);

    const { memberList } = useSelector(state => ({
        memberList: state.members.list
    }));

    const handleCreateNew = () => {
        history.push('/members/create');
    }

    return (
        <Page enableAutoDevice>
            {memberList.length > 0 ? memberList.map((value, index) => <NavLink key={index} to={'/members/view/' + value.id}><MemberComponent.ListView value={value} /></NavLink>) : <EmptyData/>}
            <FloatingActionButtons onClick={handleCreateNew}/>
        </Page>
    )
}