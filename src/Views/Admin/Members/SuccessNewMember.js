import React from 'react';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import ButtonLoader from '../../../Components/ButtonLoader';
import { NavLink, useHistory } from 'react-router-dom';

export default function SuccessNewMember(props){

    const History = useHistory();

    const registrationId = props.match.params.registrationId 

    return (
        <div>
            <div className="centered">
                <div><CheckCircleOutlineIcon className="text-success" style={{fontSize: '3rem'}}/></div>
                <div className="f-s-20 f-w-600">Successfully created.</div>
                <div className="small mt-5">
                    <div>Membership ID: <NavLink to={'/members/view/545484487'}><u className="text-primary">#{registrationId}</u></NavLink></div>
                    <div className="mt-20 small text-justify text-secondary">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    </div>
                    <div className="mt-30">
                        <div>
                            <ButtonLoader 
                                className={'text-left'}
                                onClick={() => History.push('/dashboard')} 
                                // buttonLoader={buttonUploadLoader} 
                                fullWidth={false} 
                                size="small" 
                                color="default"
                                label="Back to home"
                                style={{fontSize: 10}}
                            />
                            <ButtonLoader 
                                className={'text-left'}
                                onClick={() => History.push('/members/create')}  
                                // buttonLoader={buttonUploadLoader} 
                                fullWidth={false} 
                                style={{marginLeft: 20, fontSize: 10}}
                                size="small" 
                                color="default"
                                label="New member"
                            />
                        </div>
                        <div className="mt-20">
                            <ButtonLoader 
                                className={'text-left'}
                                onClick={() => History.push('/members/schedule-workout/878787878787878787')}
                                // buttonLoader={buttonUploadLoader} 
                                fullWidth={false} 
                                size="small" 
                                color="default"
                                style={{fontSize: 10}}
                                label="Schedule workout"
                            />
                            <ButtonLoader 
                                className={'text-left'}
                                onClick={() => History.push('/members')}
                                // buttonLoader={buttonUploadLoader} 
                                fullWidth={false} 
                                size="small" 
                                color="default"
                                style={{fontSize: 10, marginLeft: 20}}
                                label="Members list"
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}