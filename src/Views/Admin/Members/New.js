import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import MenuItem from '@material-ui/core/MenuItem';

import { BloodGroup, Nationalities, GenderData } from '../../../Data';
import AppTextField from '../../../Components/AppTextField';
import Pages from '../../../Components/Page';
import ButtonLoader from '../../../Components/ButtonLoader';
import DatePicker from '../../../Components/DatePicker';
import AppSelectBox from '../../../Components/AppSelectBox';
import SimpleReactValidator from 'simple-react-validator';
import useForceUpdate from 'use-force-update';
import { saveMembers } from '../../../Actions/Members';
import { useDispatch } from 'react-redux';

export default function New() {

    const History = useHistory();
    const forceUpdate = useForceUpdate();
    const dispatch = useDispatch();
    
    const [buttonLoader, setButtonLoader] = useState(false);

    const [validator] = React.useState(new SimpleReactValidator());
    const [firstName, setFirstName] = useState('Mohammed sadik');
    const [middleName, setMiddleName] = useState('');
    const [lastName, setLastName] = useState('VK');
    const [dob, setDob] = useState('1980-01-01T00:00:00.590+00:00');
    const [email, setEmail] = useState('sample@gmail.com');
    const [mobile, setMobile] = useState('');
    const [gender, setGender] = useState('1');
    const [emergencyContactName, setEmergencyContactName] = useState('shajan');
    const [emergencyContactNumber, setEmergencyContactNumber] = useState('9633388448');
    const [bloodGroup, setBloodGroup] = useState('1');
    const [height, setHeight] = useState('173');
    const [weight, setWeight] = useState('95');
    const [nationality, setNationality] = useState('1');
    const [permanentAddress, setPermanentAddress] = useState('Test');
    const [currentAddress, setCurrentAddress] = useState('Test');
    const [place, setPlace] = useState('Test');
    const [otherInformation, setOtherInformation] = useState('Test');
    const [error, setError] = useState({mobile: ''})

    React.useEffect(() => {
        window.scrollTo(0, 0)
    }, []);

    const submitHandler = (e) => {
        e.preventDefault();
        if (validator.allValid()) {
            setButtonLoader(true);
            dispatch(saveMembers({
                first_name: firstName,
                middle_name: middleName,
                last_name: lastName,
                dob: dob,
                email: email,
                mobile: mobile,
                gender: gender,
                emergency_contact_name: emergencyContactName,
                emergency_contact_number: emergencyContactNumber,
                blood_group: bloodGroup,
                height: height,
                weight: weight,
                nationality: nationality,
                permanent_address: permanentAddress,
                current_address: currentAddress,
                place: place,
                other_information: otherInformation,
            }))
            .then(response => {
                setButtonLoader(false);
                if(response.status === true) {
                    History.push('/members/id-details/' + response.result.id);
                }else {
                    setError({mobile: response.message})
                }
            })
        }else {
            window.scrollTo(0, 0)
            setError({mobile: ''})
            validator.showMessages();
            forceUpdate();
        }
    }

    return (
        <Pages container="container">
            <form className="form mt-20 mb-20" method="POST">
                <div className="card-title">Basic details</div>
                <div className="form-group">
                    <AppTextField value={firstName} onChange={e => setFirstName(e.target.value)} required fullWidth label="First name" placeholder="First name"/>
                    {validator.message('first name', firstName, 'required')}
                </div>
                <div className="form-group">
                    <AppTextField value={middleName} onChange={e => setMiddleName(e.target.value)} fullWidth label="Middle name" placeholder="Middle name"/>
                </div>
                <div className="form-group">
                    <AppTextField value={lastName} onChange={e => setLastName(e.target.value)} required fullWidth label="Last name" placeholder="Last name"/>
                    {validator.message('last name', lastName, 'required')}
                </div>
                <div className="form-group">
                    <DatePicker label="Date of birth" value={dob} onChange={date => setDob(date)} />
                    {validator.message('date of birth', dob, 'required')}
                </div>
                <div className="form-group">
                    <AppTextField value={email} onChange={e => setEmail(e.target.value)} fullWidth label="Email" placeholder="Email"/>
                    {validator.message('email', email, 'email')}
                </div>
                <div className="form-group">
                    <AppTextField type="number" value={mobile} onChange={e => setMobile(e.target.value)} required fullWidth label="Mobile" placeholder="Mobile"/>
                    {validator.message('mobile', mobile, 'required|numeric|max:15|min:10')}
                    <div className="error">{error.mobile}</div>
                </div>
                <div className="form-group">
                    <AppSelectBox value={gender} onChange={e => setGender(e.target.value)} required fullWidth label="Gender">
                        {GenderData().map((value, index) => <MenuItem key={index} value={value.id}>{value.name}</MenuItem>)}
                    </AppSelectBox>
                    {validator.message('gender', gender, 'required')}
                </div>
                <div className="form-group">
                    <AppTextField value={emergencyContactName} onChange={e => setEmergencyContactName(e.target.value)} fullWidth label="Emergency contact name" placeholder="Emergency contact name"/>
                </div>
                <div className="form-group">
                    <AppTextField type="number" value={emergencyContactNumber} onChange={e => setEmergencyContactNumber(e.target.value)} fullWidth label="Emergency contact number" placeholder="Emergency contact number"/>
                    {validator.message('emergency contact number', emergencyContactNumber, 'numeric|max:15|min:10')}
                </div>
                <div className="form-group">
                    <AppSelectBox value={bloodGroup} onChange={e => setBloodGroup(e.target.value)} required fullWidth label="Blood group" placeholder="Blood group">
                        {BloodGroup().map((value, index) => <MenuItem key={index} value={value.id}>{value.name}</MenuItem>)}
                    </AppSelectBox>
                    {validator.message('blood group', bloodGroup, 'required')}
                </div>
                <div className="form-group">
                    <AppTextField type="number" value={height} onChange={e => setHeight(e.target.value)} fullWidth label="Height" placeholder="Height"/>
                    {validator.message('height', height, 'numeric|between:3,3')}
                </div>
                <div className="form-group">
                    <AppTextField type="number" value={weight} onChange={e => setWeight(e.target.value)} fullWidth label="Weight" placeholder="Weight"/>
                    {validator.message('height', weight, 'numeric|max:3')}
                </div>
                <div className="form-group">
                    <AppSelectBox value={nationality} onChange={e => setNationality(e.target.value)} required fullWidth label="Nationality" placeholder="Nationality">
                        {Nationalities().map((value, index) => <MenuItem key={index} value={value.id}>{value.name}</MenuItem>)}
                    </AppSelectBox>
                    {validator.message('nationality', nationality, 'required')}
                </div>
                <div className="form-group">
                    <AppTextField value={permanentAddress} onChange={e => setPermanentAddress(e.target.value)} required fullWidth label="Permanent address" placeholder="Permanent address"/>
                    {validator.message('permanentAddress', permanentAddress, 'required')}
                </div>
                <div className="form-group">
                    <AppTextField value={currentAddress} onChange={e => setCurrentAddress(e.target.value)} fullWidth label="Current Address" placeholder="Current Address"/>
                </div>
                <div className="form-group">
                    <AppTextField value={place} onChange={e => setPlace(e.target.value)} fullWidth label="Place" required placeholder="Place"/>
                    {validator.message('place', place, 'required')}
                </div>
                <div className="form-group">
                    <AppTextField value={otherInformation} onChange={e => setOtherInformation(e.target.value)} multiline rows={5} fullWidth label="Other Information" placeholder="Other Information"/>
                </div>
                <div className="mt-20">
                    <ButtonLoader onClick={submitHandler} buttonLoader={buttonLoader} fullWidth={true} size="medium" label="Submit"></ButtonLoader>
                </div>
            </form> 
        </Pages>
    );
}