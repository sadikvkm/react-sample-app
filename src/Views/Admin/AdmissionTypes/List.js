import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Pages from '../../../Components/Page';
import HeaderHelmet from '../../../Components/HeaderHelmet';
import AppCard from '../../../Components/AppCard';
import FloatingActionButtons from '../../../Components/FloatingActionButtons';
import AppModal from '../../../Components/AppModal';
import AppTextField from '../../../Components/AppTextField';
import { 
    admissionTypes, 
    saveAdmissionTypes, 
    deleteAdmissionTypes, 
    AdmissionTypesDetails,
    updateAdmissionTypes
} from '../../../Actions/AdmissionTypes';
import ButtonLoader from '../../../Components/ButtonLoader';
import AppCheckBox from '../../../Components/AppCheckBox';
import EmptyData from '../../../Components/EmptyData';
import SimpleReactValidator from 'simple-react-validator';
import useForceUpdate from 'use-force-update';
import { getStatus } from '../../../Helpers'; 
import EditIcon from '@material-ui/icons/Edit';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import Delete from '../../../Components/Delete';

export default function List(props) {

    const dispatch = useDispatch();
    const forceUpdate = useForceUpdate();

    const [openFormModal, setOpenFormModal] = React.useState(false);
    const [buttonLoader, setButtonLoader] = React.useState(false);
    const [validator, setValidator] = React.useState(new SimpleReactValidator());
    const [admissionType, setAdmissionType] = React.useState('');
    const [amount, setAmount] = React.useState('');
    const [tax, setTax] = React.useState(false);
    const [remarks, setRemarks] = React.useState('');
    const [deleteModal, setDeleteModal] = React.useState(false);
    const [currentDeleteId, setCurrentDeleteId] = React.useState('');
    const [successDelete, setSuccessDelete] = React.useState(false);
    const [updateId, setUpdateId] = React.useState('');

    React.useEffect(() => {

        dispatch(admissionTypes())

    }, [dispatch]);

    const { list } = useSelector(state => ({
        list: state.admissionType.list
    }));

    const handleOpenForm = () => {
        setOpenFormModal(true);
        setUpdateId('')
        resetFormData()
    }

    const handleSubmit = () => {
        if (validator.allValid()) {
            setButtonLoader(true)

            if(updateId) {
                dispatch(updateAdmissionTypes(updateId, {
                    admission_type: admissionType,
                    amount: amount,
                    tax: tax,
                    remarks: remarks
                }))
                .then(response => {
                    dispatch(admissionTypes());
                    setOpenFormModal(false);
                    resetFormData();
                })
            }else {
                dispatch(saveAdmissionTypes({
                    admission_type: admissionType,
                    amount: amount,
                    tax: tax,
                    remarks: remarks
                }))
                .then(response => {
                    dispatch(admissionTypes());
                    setOpenFormModal(false);
                    resetFormData();
                })
            }



        }else {
            validator.showMessages();
            forceUpdate();
        }
    }

    const handleDeleteConfirm = (id) => {
        setDeleteModal(true)
        setCurrentDeleteId(id);
    }

    const handleDelete = () => {
        dispatch(deleteAdmissionTypes(currentDeleteId))
        .then(response => {
            dispatch(admissionTypes())
            setDeleteModal(false);
            setSuccessDelete(false);
        })
    }

    const handleEdit = (id) => {
        dispatch(AdmissionTypesDetails(id))
        .then(response => {
            setUpdateId(id)
            setOpenFormModal(true)
            setAdmissionType(response.admission_type)
            setAmount(response.amount)
            setTax(response.tax)
            setRemarks(response.remarks)
            setValidator(new SimpleReactValidator())
        })
    }

    const resetFormData = () => {
        setButtonLoader('')
        setAdmissionType('')
        setAmount('')
        setTax(false)
        setRemarks('')
        setValidator(new SimpleReactValidator())
    }

    return (
        <div>
            <HeaderHelmet title="Admission types">
                <style>{'body{ background-color: rgb(243, 243, 243) !important;'}</style>
            </HeaderHelmet>
            <Pages container="container-default">
                {list.length > 0 ? list.map((value, index) => {

                    return (
                        <div key={index} className="mb-10">
                            <AppCard>
                                <div>
                                    <div className="d-flex">
                                        <div className="card-title w-100">{value.admission_type}</div>
                                        <div className="text-right w-40">
                                            {/* <VisibilityOffIcon style={{fontSize: 17, color: 'gray'}}/> */}
                                            <EditIcon onClick={() => handleEdit(value.id)} style={{fontSize: 17, color: 'blue',  marginLeft: 13}} />
                                            <DeleteForeverIcon onClick={() => handleDeleteConfirm(value.id)} style={{fontSize: 17, color: 'red', marginLeft: 13}} />
                                        </div>
                                    </div>
                                    <div className="f-s-12 text-secondary">
                                        <div>Tax: {value.tax ? 'Yes' : 'No'}, Amount: {value.amount}, status: <span className="text-success">{getStatus(value.status)}</span></div>
                                        <div className="text-justify">
                                            {value.remarks}
                                        </div>
                                    </div>
                                </div>
                            </AppCard>
                        </div>
                    )
                }) : <EmptyData/>}
            </Pages>
            <FloatingActionButtons onClick={() => handleOpenForm()}/>
            <AppModal 
                open={openFormModal} 
                close={() => setOpenFormModal(false)} 
                title="Admission types"
                actionButton={
                    <ButtonLoader onClick={handleSubmit} buttonLoader={buttonLoader} fullWidth={true} size="small" label="Submit"></ButtonLoader>
                }
            >
               <form className="form">
                    <div className="form-group">
                        <AppTextField value={admissionType} onChange={(e) => setAdmissionType(e.target.value)} fullWidth placeholder="Admission type" label="Admission type" required />
                        {validator.message('admission type', admissionType, 'required')}
                    </div>
                    <div className="form-group">
                        <AppTextField value={amount} onChange={(e) => setAmount(e.target.value)} type="number" fullWidth placeholder="Amount" label="Amount" required />
                        {validator.message('amount', amount, 'required|numeric')}
                    </div>
                    <div className="form-group">
                        <AppCheckBox checked={tax} onChange={e => setTax(e.target.checked)} label="Tax" />
                    </div>
                    <div className="form-group">
                        <AppTextField value={remarks} onChange={(e) => setRemarks(e.target.value)} multiline rows={4} fullWidth placeholder="Remarks" label="Remarks" />
                    </div>
               </form>
            </AppModal>
            <Delete open={deleteModal} close={() => setDeleteModal(false)} delete={handleDelete} success={successDelete} />
        </div>
    )
}