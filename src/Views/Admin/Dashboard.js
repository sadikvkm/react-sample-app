import React from 'react';
import HeaderHelmet from '../../Components/HeaderHelmet';
import AppCard from '../../Components/AppCard';
import { makeStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
import Pages from '../../Components/Page';
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import AccountBoxIcon from '@material-ui/icons/AccountBox';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import AssignmentTurnedInIcon from '@material-ui/icons/AssignmentTurnedIn';
import ImageAvatars from '../../Components/ImageAvatars';
import EventNoteIcon from '@material-ui/icons/EventNote';
import { NavLink } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
    menus: {
        marginTop: theme.spacing(3),
        textAlign: 'center',
        '& svg': {
            color: '#fff'
        },
        '& .d-flex .w-100 div': {
            color: '#fff',
            fontSize: 12,
            marginTop: 5
        }
    },  
    eventSection: {
        '& svg': {
            color: '#000',
            fontSize: '2rem',
        },
        '& > div': {
            width: '100%'
        },
    },
    appCard: {
      marginTop: theme.spacing(3),
      '& .MuiPaper-rounded': {
        borderRadius: '14px 14px 0px 0px'
      }
    },
}));

export default function Dashboard(props) {

    const classes = useStyles();

    return (
        <div>
            <HeaderHelmet title="Dashboard">
                <style>{'body{ background-color: rgb(243, 243, 243) !important;'}</style>
            </HeaderHelmet>
            <Pages enableAutoDevice>
                <div style={{background: 'rgb(63, 81, 181)', display: 'flow-root'}}>
                    <div>
                        <div className={classes.menus}>
                            <div className="d-flex">
                                <NavLink  to={"/members/create"} className="w-100">
                                    <Fab disabled aria-label="like">
                                        <AssignmentIndIcon />
                                    </Fab>
                                    <div>Admission</div>
                                </NavLink>
                                <NavLink to={"/members"} className="w-100">
                                    <Fab disabled aria-label="like">
                                        <AccountBoxIcon />
                                    </Fab>
                                    <div>Members</div>
                                </NavLink>
                                <div className="w-100">
                                    <Fab disabled aria-label="like">
                                        <CheckCircleIcon />
                                    </Fab>
                                    <div>Attendance</div>
                                </div>
                                <NavLink to={'/courses'} className="w-100">
                                    <Fab disabled aria-label="like">
                                        <AssignmentTurnedInIcon />
                                    </Fab>
                                    <div>Courses</div>
                                </NavLink>
                            </div>
                        </div>
                    </div>
                    <div className={classes.appCard}>
                        <AppCard>
                            <div className="mt-10">
                                <div className={classes.eventSection + " mt-10 small d-flex text-center"}>
                                    <div>
                                        <div className="f-bold">Members</div>
                                        <div>1000</div>
                                    </div>
                                    <div>
                                        <div className="f-bold">This Months</div>
                                        <div>1000</div>
                                    </div>
                                </div>
                                <div className="divider mt-20 mb-10"></div>
                                <div>
                                    <div className="d-flex">
                                        <div className="w-100">
                                            <span className="card-title">Upcoming events</span>
                                        </div>
                                        <div className="w-50 text-right">
                                            <div className="small">View all</div>
                                        </div>
                                    </div>
                                    <div className="p-40 text-center text-secondary small">
                                        <EventNoteIcon style={{fontSize: '3rem'}} />
                                        <div>No events.</div>
                                    </div>
                                </div>
                            </div>
                        </AppCard>
                    </div>
                </div>
                <div className="mt-10">
                    <div style={{background: 'rgb(225, 225, 225)'}}>
                        <AppCard container={false}>
                            <div className="d-flex p-15">
                                <div className="w-100">
                                    <span className="card-title">Pending fees</span><span className="small ml-10 text-secondary">28, Aug 2020</span>
                                </div>
                                <div className="w-50 text-right">
                                    <div className="small">View all</div>
                                </div>
                            </div>
                            {[1,2,3,4].map((value, index) => {

                                return (
                                    <div key={index}>
                                        <div className="p-10 d-flex">
                                            <ImageAvatars />
                                            <div className="mt-10 w-100">
                                                <div className="small f-w-600">Mohammed sadik vk</div>
                                                <div className="small text-secondary">Join date: 20, Jan 2020</div>
                                            </div>
                                            <div className="w-50 mt-10 text-right mr-5">
                                                <div className="text-danger small">500.00</div>
                                            </div>
                                        </div>
                                        <div className="divider"></div>
                                    </div>
                                )
                            })}
                        </AppCard>
                    </div>
                </div>
            </Pages>
        </div>
    )
}