import React from 'react';

export function getStatus(status) {
    
    let color = 'success';
    let label = 'Active';
    if(! status) {
        label = 'Inactive';
        color = 'text-danger';
    }

    return <span className={color}>{label}</span>;
}

export function fullName(data) {


    return data.first_name + ' ' + data.middle_name + ' ' + data.last_name
}