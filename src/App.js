import React from 'react';
import { 
  BrowserRouter as Router, 
  Route, 
  Switch,
  Redirect,
} from "react-router-dom";

import * as Authentication from './Views/Auth';
import * as Admin from './Views/Admin';
import * as Members from './Views/Admin/Members';
import * as Courses from './Views/Admin/Courses';
import * as AdmissionTypes from './Views/Admin/AdmissionTypes';
import * as MembershipPlans from './Views/Admin/MembershipPlans';

import AdminHeader from './Views/Layouts/Admin/Header';
import { Auth } from './Services/Auth';

import './App.css';
import './Template.css';

const PrivateRoute = ({ component: Component, data, ...rest }) => (
  <AdminHeader>
    <Route {...rest} render={(props) => (
     Auth.getToken() === false
        ? <Redirect to={{
          pathname: '/',
          state: { from: props.location }
        }} />
        :
        <Component {...props} />
    )} />
  </AdminHeader>
)

const PublicRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={(props) => (
    Auth.getToken() === false
      ? <Component {...props} />
      :
      <Redirect to={{
        pathname: '/dashboard',
        state: { from: props.location }
      }} />

  )} />
)


function App() {
  return (
    <div>
      <Router>
        <Switch>
          <PublicRoute path="/" component={Authentication.Login} exact />
          <PrivateRoute path="/dashboard" component={Admin.Dashboard} />
          <PrivateRoute path="/members" component={Members.List} exact />
          <PrivateRoute path="/members/view/:id" component={Members.View}/>
          <PrivateRoute exact path="/members/create" component={Members.New} />
          <PrivateRoute path="/members/id-details/:memberId" component={Members.IdForm}/>
          <PrivateRoute path="/members/create/success/:registrationId" component={Members.SuccessNewMember}/>
          <PrivateRoute path="/members/admission-payment/:memberId" component={Members.AdmissionPayment}/>
          <PrivateRoute path="/courses" component={Courses.List}/>
          <PrivateRoute path="/admission-types" component={AdmissionTypes.List}/>
          <PrivateRoute path="/membership-plan" component={MembershipPlans.List}/>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
