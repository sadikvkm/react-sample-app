/* eslint-disable no-use-before-define */

import React from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    multipleSelect: {

        '& .MuiChip-label': {
            fontSize: 10
        },
        '& input::placeholder': {
            fontSize: 12
        }

    },
    option: {
        '&[aria-selected="true"]': {
          backgroundColor: 'transparent',
        },
        '&[data-focus="true"]': {
          backgroundColor: theme.palette.action.hover,
        },
    },
    paper: {
        boxShadow: 'none',
        margin: 0,
        color: '#586069',
        fontSize: 13,
    },
    popperDisablePortal: {
        position: 'relative',
    },          
}));

const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
const checkedIcon = <CheckBoxIcon fontSize="small" />;

export default function CheckboxesTags(props) {

    const classes = useStyles();

    return (
        <Autocomplete
            multiple
            id="checkboxes-tags-demo"
            options={props.data.length > 0 ? props.data : []}
            disableCloseOnSelect
            defaultValue={props.defaultValue}
            getOptionLabel={(option) => option.title}
            onChange={(event, value) => props.onChange(value, event)}
            size="small"
            classes={{
                paper: classes.paper,
                option: classes.option,
                popperDisablePortal: classes.popperDisablePortal,
            }}
            renderOption={(option, { selected }) => (
                <React.Fragment>
                <Checkbox
                    icon={icon}
                    checkedIcon={checkedIcon}
                    checked={selected}
                />
                {option.title}
                </React.Fragment>
            )}
            renderInput={(params) => (
                <TextField {...params} size="small" variant="outlined" label="Checkboxes" placeholder={props.placeholder} />
            )}
        />
    );
}