import React from 'react';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    formControl: {
        // marginTop: '5px !important',
        // marginBottom: '5px !important'
    },
    inputLabel: {
        // color: "red",
        fontSize: 15,
        color: '#2f2f2f2',
        "&$inputFocused": {
        //   color: "orange"
        }
    },
    inputFocused: {},
    select: {
        fontSize: 13,
        color: "black",
        "&:before": {
          // changes the bottom textbox border when not focused
        //   borderColor: "red"
        },
        "&:after": {
          // changes the bottom textbox border when clicked/focused.  thought it would be the same with input label
        //   borderColor: "green"
        }
    },
    dropdownStyle: {
        // border: "1px solid black",
        // borderRadius: "5%",
        backgroundColor:'#fff',
        fontSize: 13,
        '& li.MuiListItem-button': {
            fontSize: 12
        }
    },
}));

export default function AppSelectBox(props) {

    const classes = useStyles();
    
    return (
        <div className={props.className} style={props.style ? props.style : {}}>
            <FormControl fullWidth={props.fullWidth} size={props.size} variant={props.variant} className={classes.formControl}>
                <InputLabel 
                classes={{ focused: classes.inputFocused }}
                className={classes.inputLabel}
                required={props.required}
                id={props.id + "-select-outlined-label"}>{props.label}</InputLabel>
                <Select
                    labelId={props.id + "-select-outlined-label"}
                    id={props.id}
                    label={props.label}
                    value={props.value}
                    className={classes.select}
                    defaultValue=''
                    onChange={(e) => props.onChange(e)}
                    inputlabelprops={{
                        classes: {
                            root: classes.floatingLabelFocusStyle,
                            focused: "focused"
                        }
                    }}
                    MenuProps={{ classes: { paper: classes.dropdownStyle, label: classes.selectLabel } }}
                >
                <MenuItem value=''>
                    <em>None</em>
                </MenuItem>
                    {props.children}
                </Select>
            </FormControl>
        </div>
    )
    
}