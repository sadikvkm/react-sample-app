import React from 'react';
import Container from '@material-ui/core/Container';
// import { makeStyles } from '@material-ui/core/styles';
import Template from '../Config/Template';
import {
    BrowserView,
    MobileView,
    // isBrowser,
    // isMobile
  } from "react-device-detect";

// const useStyles = makeStyles(theme => ({

//     wrapper: {

//         height: '12rem'
//     },
// }));

export default function Pages(props) {

    // const classes = useStyles();

    const normalContainer = () => {

        return (
            <div style={{padding: 10}}>{props.children}</div>
        )
    } 

    const container = () => {

        return (
            <Container maxWidth={(props.maxWidth) ? props.maxWidth : Template.maxWidth}>{props.children}</Container>
        )
    }

    const mobileContainer = () => {

        return (
            <div>
                <BrowserView>
                    <Container maxWidth="sm">{props.children}</Container>
                </BrowserView>
                <MobileView>
                    {props.children}
                </MobileView>
            </div>
        )
    }

    const getContainer = () => {
        
        if(props.enableAutoDevice) {

            return mobileContainer();
        }

        let pass = ""

        switch (props.container) {

            case 'container-default':
                
                pass = normalContainer();

                break;
            
            case 'container':
                
                pass = container();

                break;
        
            default:
                pass = props.children;
                break;
        }

        return pass;
    }

    return getContainer()
}