import React from 'react';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';

export default function AppSelectBox(props) {

    const getOptions = () => {

        if(props.children  !== undefined) {

            return props.children
        }else {

            return (

                props.options.map((value, index) => {
                    return (
                        <MenuItem key={index} value={value.id}>{value.value}</MenuItem>
                    )
                })
                
            )
        }
    }

    return (
        <FormControl fullWidth size="small" variant="outlined">
            <InputLabel id={ props.label + "district-select-outlined-label"}>{props.label}</InputLabel>
            <Select
                labelId={ props.label + "district-select-outlined-label"}
                multiple={props.multiple}
                id={ props.label + "district-select-outlined"}
                value={props.value}
                onChange={(e) => props.onChange(e)}
                label={props.label}
            >
            <MenuItem disabled value="">
                <em>None</em>
            </MenuItem>

                {getOptions()}
                
            </Select>
        </FormControl>
    )
    
}