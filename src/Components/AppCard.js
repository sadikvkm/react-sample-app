import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

export default function AppCard(props) {

    const initCard = () => {

        if(props.container === false) {

            return <div id="free-card">{props.children}</div>
        }

        return (
            <CardContent>
                {props.children}
            </CardContent>
        )
        
    }

    return (
        <Card style={(props.style) ? props.style : {}}>
            {initCard()}
        </Card>
    )
}