import React from 'react';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import 'date-fns';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    textField: {
        // marginTop: '5px !important',
        // marginBottom: '5px !important'
    },
    notchedOutline: {
        borderWidth: "1px",
        fontSize: 12,
        borderColor: "rgb(196, 196, 196) !important"
    },
    cssOutlinedInput: {
        fontSize: 14,
    },
    floatingLabelFocusStyle: {
        fontSize: 14,
        color: '#2f2f2f2',
    }
}));

export default function TimePicker(props) {

    const classes = useStyles();

    return (
        <MuiPickersUtilsProvider variant="outlined" utils={DateFnsUtils}>
            <KeyboardDatePicker
                disabled={props.disabled}
                margin="normal"
                format="dd/MM/yyyy"
                id={props.id}
                label={props.label}
                value={(props.value) ? props.value : new Date()}
                onChange={props.onChange}
                fullWidth
                size="small"
                // inputVariant="outlined"
                KeyboardButtonProps={{
                    'aria-label': 'change time',
                }}
                InputLabelProps={{
                    classes: {
                        root: classes.floatingLabelFocusStyle,
                        focused: "focused"
                        }
                }}
                InputProps={{
                    classes: {
                        root: classes.cssOutlinedInput,
                        // notchedOutline: classes.notchedOutline
                    }
                }}
            />
        </MuiPickersUtilsProvider>
    )
}