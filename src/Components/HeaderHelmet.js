import React from 'react';
import { Helmet } from "react-helmet-async";
import Config from '../Config';

export default function HeaderHelmet(props) {

    return (
        <div>
            <Helmet>
                <title>{Config.APP_NAME + ' - ' + props.title}</title>
                {props.children}
            </Helmet>
        </div>
    )
}