import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Icon from '@material-ui/core/Icon';

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'fixed',
    bottom: '10px',
    right: '10px',
    '& > *': {
      margin: theme.spacing(1),
    },
  }
}));

export default function FloatingActionButtons(props) {
  const classes = useStyles();

  const handleClick = (e) => {

    props.onClick(e);
  }

  const actionIcon = () => {

    if(props.icon) {

        return <Icon>{props.icon}</Icon>
    }
      
    return <AddIcon />;
  }

  return (
    <div className={classes.root}>
      <Fab onClick={handleClick} color={props.color ? props.color : "primary"} aria-label="add">
        {actionIcon()}
      </Fab>
    </div>
  );
}