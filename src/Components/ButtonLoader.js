import React from 'react';
import Button from '@material-ui/core/Button';
import Loader from './Loader';

export default function ButtonLoader(props) {
    
    const intLoader = () => {
        
        return (<Loader className={props.className ? props.className : "text-center"} type="Bars" height={(props.height) ? props.height : 25} width={(props.width) ? props.width : 50} color="#000" />);
    }

    const handleClick = (e) => {

        e.preventDefault();
        props.onClick(e);
    }

    const initSubmitButton = () => {
        if(props.customButton) {

            return props.customButton;
        }

        let fullWidth = true;

        if(fullWidth) {

            if(props.fullWidth === false) {
                fullWidth = false;
            }
        }

        return (
            <Button
                onClick={handleClick}
                style={(props.style) ? props.style : {}}
                type="submit"
                startIcon={props.startIcon ? props.startIcon : ''}
                fullWidth={fullWidth}
                variant="contained"
                color={props.color ? props.color : "primary"}
                size={props.size ? props.size : 'medium'}
                className={(props.className) ? props.className : ''}
                disabled={(props.disabled) ? props.disabled : false}
            >
                {props.label ? props.label : 'Submit'} 
            </Button>
        )
    }

    const handleEvent = () => {

        if(props.buttonLoader === true) {

            return intLoader();
        }
        return initSubmitButton();
    }

    return handleEvent();
}