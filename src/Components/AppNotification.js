import React from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';
import { AppNotification } from '../Actions/AppNotification';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
}));

export default function CustomizedSnackbars() {
    const classes = useStyles();
    const dispatch = useDispatch();

    const [state] = React.useState({
    open: false,
    vertical: 'bottom',
    horizontal: 'center',
  });

  const { vertical, horizontal } = state;

    const { notification } = useSelector(state => ({
        notification: state.appNotification
    }))    

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        dispatch(AppNotification(false));
    };

    const setNotification = () => {

        if(notification.notify) {
            return (
                <Snackbar
                    anchorOrigin={{ vertical, horizontal }}
                    key={`${vertical},${horizontal}`}
                    open={true}
                    onClose={handleClose}
                    message="Please wait..."
                />
            )
        }else {
            return (
                <Snackbar
                open={notification.open} 
                autoHideDuration={2000} 
                onClose={handleClose}>
                    <Alert onClose={handleClose} severity={notification.type}>
                        {notification.message}
                    </Alert>
                </Snackbar>
            )
        }
    }

    return (
        <div className={classes.root}>
            {setNotification()}
        </div>
    );
}