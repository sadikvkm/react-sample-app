
import React from "react";
import Geolocation from "react-geolocation";
import { useDispatch } from 'react-redux';
import { fetchLocationDetails } from '../Actions/Application';
import useGeolocation from 'react-hook-geolocation'

export default function GeoLocation() {

    const dispatch = useDispatch();

    const geolocation = useGeolocation();
    
    React.useState(() => {
        // console.log('test')

    })

    // console.log(geolocation)


    
    const setLocation = (data) => {

        // console.log(data.coords.latitude,  data.coords.longitude)

        dispatch(fetchLocationDetails(data.coords.latitude,  data.coords.longitude));
    }

    return (
        <Geolocation onSuccess={setLocation} />
    )
    
}

