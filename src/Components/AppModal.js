import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';

export default function AppModal(props) {

  const [fullWidth] = React.useState(true);
  const [maxWidth] = React.useState('sm');

  return (
    <React.Fragment>
      <Dialog
        id={props.id}
        classes={(props.classes ? props.classes : {})}
        fullWidth={fullWidth}
        maxWidth={maxWidth}
        open={props.open}
        fullScreen={props.fullScreen}
        onClose={props.close}
        aria-labelledby="max-width-dialog-title"
      >
        <div className="card-title p-20" style={{marginBottom: '-15px', marginLeft: 4}}>{props.title}</div>
        {(props.dialogContent === false) ? 
          <div>{props.children}</div>
          :
          <DialogContent>

            {props.children}
            
          </DialogContent>
        } 
        <DialogActions>
        {(props.dialogContent === false) ? null :
          <div style={{marginRight: '16px', display: 'flex', marginBottom: 10}}>
            <Button size="small" onClick={props.close} color="primary">
              Close
            </Button>
            {props.actionButton}
          </div>
        }
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
}