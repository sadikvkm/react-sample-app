import React from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';

export default function AppSwitchButton(props) {

    return (

        <FormControlLabel
            control={
                <Switch
                    checked={props.checked}
                    onChange={props.onChange}
                    name={props.name}
                    color={props.color ? props.color : 'default'}
                    className={props.className}
                />
            }
            label={props.label}
        />
    )
}