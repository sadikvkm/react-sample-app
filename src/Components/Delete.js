import React from 'react';
import AppModal from './AppModal';
import ButtonLoader from './ButtonLoader';

export default function MyOrderView(props) {

    const [buttonLoader, setButtonLoader] = React.useState(false);

    

    const handleSubmit = () => {
        setButtonLoader(true)
        props.delete()
    }

    const getButtonLoader = () => {

        if(props.open === false) {

            if(buttonLoader === true) 
                setButtonLoader(false);

            return false;
        }
        return buttonLoader;
    }

    return (
        <AppModal
            actionButton={
                <ButtonLoader color="secondary" label="Delete" onClick={handleSubmit} buttonLoader={getButtonLoader()} fullWidth={true} size="small"></ButtonLoader>
            }
            close={() => props.close()} 
            title="Promp!" 
            open={props.open}>
            <div className="text-center"><h4>Are your sure want delete?</h4></div>
        </AppModal>
    )
}