import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  small: {
    width: theme.spacing(3),
    height: theme.spacing(3),
  },
  large: {
    width: theme.spacing(7),
    height: theme.spacing(7),
  },
}));

export default function ImageAvatars(props) {
  const classes = useStyles();

  const getImage = () => {

    switch (props.size) {
        case 'small':
            return <Avatar alt="Remy Sharp" src="https://material-ui.com/static/images/avatar/1.jpg" className={classes.small} />;
    
        case 'large':
           return <Avatar alt="Remy Sharp" src={props.image} className={classes.large} />;
            
        default:
            return <Avatar alt="Remy Sharp" src="https://material-ui.com/static/images/avatar/1.jpg" />;
    }
  }

  return (
    <div className={classes.root}>
        {getImage()}
    </div>
  );
}