import React from 'react';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    checkBox: {
        
        '& .MuiTypography-root': {
            fontSize: 14
        }
    }
}));

export default function AppCheckBox(props) {

    const classes = useStyles();

    return (
        <FormGroup row>
            <FormControlLabel
                className={classes.checkBox}
                onChange={props.onChange}
                checked={props.checked}
                control={
                <Checkbox
                    
                    color={props.color ? props.color : 'primary'}
                    icon={<CheckBoxOutlineBlankIcon fontSize="small" />}
                    checkedIcon={<CheckBoxIcon fontSize="small" />}
                    name={props.name}
                />
                }
                label={props.label}
            />
        </FormGroup>
    );
}