import React from 'react';
import Loader from 'react-loader-spinner'

export default function AppLoader(props) {
    //other logic
    return(
        <div style={{padding: props.padding}}>
            <Loader
                className={props.className}
                type={props.type}
                color={props.color}
                height={props.height}
                width={props.width}
                // timeout={3000} //3 secs
            />
        </div>
    );
}