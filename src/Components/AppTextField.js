import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles(theme => ({
    textField: {
        // marginTop: '5px !important',
        // marginBottom: '5px !important'
    },
    notchedOutline: {
        borderWidth: "1px",
        fontSize: 12,
        borderColor: "rgb(196, 196, 196) !important"
    },
    cssOutlinedInput: {
        fontSize: 14
    },
    floatingLabelFocusStyle: {
        fontSize: 14,
        color: '#2f2f2f2',
    }
}));

export default function AppTextField(props) {

    const classes = useStyles();

    return (
        <div className={props.className} style={props.style ? props.style : {}}>
            <TextField 
                {...props.params}
                id={props.id} 
                required={props.required}
                onBlur={props.onBlur}
                variant={props.variant}
                fullWidth={props.fullWidth} 
                size={props.size}
                multiline={props.multiline ? true : false}
                rows={props.rows}
                onChange={(e) => props.onChange(e)}
                value={props.value}
                type={props.type}
                placeholder={props.placeholder}
                className={classes.textField + " " + props.className}
                autoFocus={props.autoFocus ? true : false}
                InputLabelProps={{
                    classes: {
                        root: classes.floatingLabelFocusStyle,
                        focused: "focused"
                    }
                }}
                InputProps={{
                    classes: {
                        root: classes.cssOutlinedInput,
                        // notchedOutline: classes.notchedOutline
                    }
                }}
                label={props.label} 
            />
        </div>
    )
}