import React from 'react';
import Moment from 'react-moment';

export default function DateFormat(props) {
    

    if(props.time === true)
        return <Moment format="hh:mm A">{props.date}</Moment>;

    if(props.dateOnly === true)
        return <Moment format="DD, MMMM YYYY">{props.date}</Moment>;

    if(props.dateTime === true)
        return <Moment format="DD, MMMM YYYY hh:mm A">{props.date}</Moment>;

    return;
}   