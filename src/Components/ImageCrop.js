import ReactDOM from 'react-dom';
import React, { PureComponent } from 'react';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import AppModal from './AppModal';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

class ImageCropper extends PureComponent {

    constructor (props) {
        super(props);
    }

    state = {
        openModal: false,
        cropImage: '',
        setCropImage: '',
        src: null,
        crop: {
            unit: '%',
            width: 300,
            aspect: 16 / 9,
            x: 0,
            y: 10,
            height: 350,
        },
    };

    onSelectFile = e => {
        if (e.target.files && e.target.files.length > 0) {
            const reader = new FileReader();
            reader.addEventListener('load', () =>
                this.setState({ src: reader.result })
            );
            this.setState({openModal: true});
            reader.readAsDataURL(e.target.files[0]);
        }
        e.target.value = null;
    };

    // If you setState the crop in here you should return false.
    onImageLoaded = image => {
        this.imageRef = image;
        this.setState({cropImage: image})
    };

    onCropComplete = crop => {
        this.makeClientCrop(crop);
    };

    onCropChange = (crop, percentCrop) => {
        // You could also use percentCrop:
        // this.setState({ crop: percentCrop });
        this.setState({ crop });
    };

    async makeClientCrop(crop) {
        if (this.imageRef && crop.width && crop.height) {
            const croppedImageUrl = await this.getCroppedImg(
                this.imageRef,
                crop,
                'image-cropped.jpg'
            );
            this.setState({ croppedImageUrl });
        }
    }

    getCroppedImg(image, crop, fileName) {
        const canvas = document.createElement("canvas");
        const scaleX = image.naturalWidth / image.width;
        const scaleY = image.naturalHeight / image.height;
        var originWidth = crop.width * scaleX;
        var originHeight = crop.height * scaleY;
        // maximum width/height
        var maxWidth = 1200, maxHeight = 1200 / (16 / 9);
        var targetWidth = originWidth,
        targetHeight = originHeight;
        if (originWidth > maxWidth || originHeight > maxHeight) {
        if (originWidth / originHeight > maxWidth / maxHeight) {
            targetWidth = maxWidth;
            targetHeight = Math.round(maxWidth * (originHeight / originWidth));
        } else {
            targetHeight = maxHeight;
            targetWidth = Math.round(maxHeight * (originWidth / originHeight));
        }
        }
        // set canvas size
        canvas.width = targetWidth;
        canvas.height = targetHeight;
        const ctx = canvas.getContext("2d");

        ctx.drawImage(
        image, 
        crop.x * scaleX, 
        crop.y * scaleY, 
        crop.width * scaleX, 
        crop.height * scaleY, 
        0, 
        0, 
        targetWidth, 
        targetHeight 
        );

        return new Promise((resolve, reject) => {

            const reader = new FileReader()
            canvas.toBlob(blob => {
                if (!blob) {
                    //reject(new Error('Canvas is empty'));
                    console.error('Canvas is empty');
                    return;
                }
                reader.readAsDataURL(blob)
                reader.onloadend = () => {
                    this.dataURLtoFile(reader.result, 'image-cropped.jpg')
                }
                blob.name = fileName;
                window.URL.revokeObjectURL(this.fileUrl);
                this.fileUrl = window.URL.createObjectURL(blob);
                resolve(this.fileUrl);
            }, 'image/jpeg');
        });
    }

    dataURLtoFile = (dataUrl, filename) => {

        let arr = dataUrl.split(','),
            mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]), 
            n = bstr.length, 
            u8arr = new Uint8Array(n);
                
        while(n--){
            u8arr[n] = bstr.charCodeAt(n);
        }
        let croppedImage = new File([u8arr], filename, {type:mime});
        this.setState({croppedImage: croppedImage }) 
    }

    onCropHandle = () => {

        this.props.result({
            file: this.state.croppedImage,
            tempUrl: this.state.croppedImageUrl
        });
        this.setState({
            setCropImage: this.state.croppedImageUrl,
            openModal: false,
        });
    }

    handleRemoveFile = () => {

        this.props.result({
            file: '',
            tempUrl: ''
        });

        this.setState({
            setCropImage: '',
        });
    }

    

    render() {
        const { crop, croppedImageUrl, src } = this.state;

        return (
        <div className="App"> 
            {(this.state.setCropImage) ? 
            
                <div>
                    <img style={{width: 80, height: 60}} src={this.state.setCropImage} />
                    <div className="mt-5 f-s-12 text-center" style={{marginTop: '-3px'}} onClick={() => this.handleRemoveFile()}>Remove</div>
                </div>
        
            :
                
                <div>
                    <label htmlFor="image-crop-block" style={{
                        padding: 10,
                        backgroundColor: 'rgb(222, 222, 222)',
                        color: 'rgb(166, 166, 166)',
                        width: 80,
                        height: 60,
                        borderRadius: '2px',
                        display: 'block',
                        textAlign: 'center'
                    }}>
                        <i className="fa fa-camera" style={{marginTop: 15}} aria-hidden="true"></i>
                        <div className="f-s-10 mt-5">Add Image</div>
                    </label>
                    <input id="image-crop-block" className="hidden" type="file" accept="image/*" onChange={this.onSelectFile} />
                </div>
            
            }
            
            
            
            
            <AppModal
            handleClose={() => this.setState({setCropImage: false})}
            actionButton={
                <Button onClick={this.onCropHandle} variant="contained" size="small" color="primary">
                    Crop
                </Button>
            }
            open={this.state.openModal}>
                <div>
                    <Grid container spacing={3}>
                        <Grid item xs={12}>
                            {src && (
                                <ReactCrop
                                    src={src}
                                    crop={crop}
                                    ruleOfThirds
                                    onImageLoaded={this.onImageLoaded}
                                    onComplete={this.onCropComplete}
                                    onChange={this.onCropChange}
                                />
                            )}
                        </Grid>
                    </Grid>
                    <Grid container spacing={3}>
                        <Grid item xs={12}>
                            {croppedImageUrl && (
                                <img alt="Crop" className="w-100" src={croppedImageUrl} />
                            )}
                        </Grid>
                    </Grid>
                </div>
            </AppModal>
        </div>
        );
    }
}

export default ImageCropper;
