import React from 'react';
import PhotoLibraryIcon from '@material-ui/icons/PhotoLibrary';

export default function EmptyData(props) {

    return (

        <div className="center-display text-center" style={{color: 'rgb(184, 184, 184)'}}>
            <div style={{fontSize: '5rem', marginBottom: '-15px'}}>
                <PhotoLibraryIcon style={{fontSize: '5rem'}} />
            </div>
            <div className="f-s-14">{(props.label) ? props.label : 'No content found.'}</div>
        </div>
    )
}