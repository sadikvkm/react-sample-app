import React from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';


const useStyles = makeStyles(theme => ({
      
    paper: {
        boxShadow: 'none',
        margin: 0,
        color: '#586069',
        fontSize: 13,
    }
      
}));

const top100Films = [
    { label: 'The Godfather fdsfdsf dsfsd f sdf dsf sdf ds fsd f sdf sd f sdf sdf sd f sdfs df sdf sd fsd f sdf s fThe Godfather fdsfdsf dsfsd f sdf dsf sdf ds fsd f sdf sd f sdf sdf sd f sdfs df sdf sd fsd f sdf s fThe Godfather fdsfdsf dsfsd f sdf dsf sdf ds fsd f sdf sd f sdf sdf sd f sdfs df sdf sd fsd f sdf s fThe Godfather fdsfdsf dsfsd f sdf dsf sdf ds fsd f sdf sd f sdf sdf sd f sdfs df sdf sd fsd f sdf s f', id: 1972 },
    { label: 'The Godfather: Part II', id: 1974 },
    { label: 'The Dark Knight', id: 2008 }
]

export default function AutoComplete(props) {

    const classes = useStyles();
    const [eventValue, setEventValue] = React.useState();

    const handleChange = (e, type = "") => {

        let set = e;
        if(typeof e === 'string') {
            set = { label: e, id: null }
        }

        if(set)
            if(set.label === "") {
                set = "";
            }
            
        props.onChange(set);

        console.log(set)
    }
    return (

        <Autocomplete
            id={props.id}
            options={top100Films}
            getOptionLabel={(option) => option.label}
            onChange={(event, value) => handleChange(value)}
            freeSolo
            inputValue={props.value}
            size="small"
            classes={{
                paper: classes.paper,
                option: classes.option,
                popperDisablePortal: classes.popperDisablePortal,
            }}
            renderInput={(params) => <TextField 
                onChange={(e) => handleChange(e.target.value)} 
                {...params} 
                label={props.label} 
                variant="outlined" 
                />
            }
        />
    )
}