import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import DefaultCss from '../Asset/Css/DefaultCss';
import Container from '@material-ui/core/Container';

const useStyles = makeStyles(theme => ({
    paper: {
        marginTop: theme.spacing(20),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: 20,
    }
}));

export default function InnerPage(props) {

    const classes = useStyles();
    const appCss = DefaultCss();

    return (
        <Container className="mt-50">
            <div className={appCss.innerPageTitle + ' mb-30'}>{props.title}<div></div></div>
            {props.children}
        </Container>
    )

}