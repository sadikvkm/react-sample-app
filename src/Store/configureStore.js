import { combineReducers } from "redux";

import appNotification from '../Reducers/AppNotificationReducer';
import admissionType from '../Reducers/AdmissionTypeReducer';
import membershipPlan from '../Reducers/MembershipPlanReducer';
import members from '../Reducers/MemberReducer';
import application from '../Reducers/ApplicationSettingsReducer';

export default combineReducers({
    appNotification,
    admissionType,
    membershipPlan,
    members,
    application
});