import AppData from './application-data.json'

export function BloodGroup(id = '') {
    
    if(id) {
        return findData(AppData.bloodGroup, id);
    }
    return AppData.bloodGroup
}

export function GenderData(id = '') {
    
    if(id) {
        return findData(AppData.gender, id);
    }
    return AppData.gender
}

export function Nationalities(id = '') {

    if(id) {
        return findData(AppData.nationalities, id);
    }
    return AppData.nationalities
}

export function IdTypes(id = '') {

    if(id) {
        return findData(AppData.idTypes, id);
    }
    return AppData.idTypes
}

export function ListMonths() {

    let array = [];
    for (let i = 1; i < 12; i++) {
        array.push(i)
    }
    console.log(array)
    return array;
}


function findData(data, id) {
    
    return data.find(i => i.id === id);
}