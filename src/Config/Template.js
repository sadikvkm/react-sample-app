// let defaultColor = '#607d8b';

module.exports = {

    APP_TEMPLATE: {
        CONTAINER: "lg",
        ADMIN: {
            headerColor: '#fff',
            app_bar: {
                color: '#000'
            }
        },
        LOGIN: {
            backgroundColor: '#fff'
        },
        FRONT_END_BANNER: "https://wallpaperplay.com/walls/full/5/2/d/186056.jpg",
    }
}