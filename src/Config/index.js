module.exports = {

    APP_NAME: 'MyGym',
    APP_LANGUAGE: 'ml',
    APP_LOGO: require('../Assets/Images/Logo.png'),
    API_URL: 'http://192.168.225.209:3031/api',
    RESOURCE_URL: 'http://192.168.225.209:3031/',
    SOCKET_URL: 'http://192.168.225.209:3031',
}   