import { PostData } from '../Services/PostData';
import { AppNotification, AppNotificationLoading } from './AppNotification';

export const SAVE_ORGANIZATION_SETTINGS = 'SAVE_ORGANIZATION_SETTINGS';


export const saveOrganizationSettings = data => ({

    type: SAVE_ORGANIZATION_SETTINGS,
    payload: data
});

export function makePayment(data) {
    return dispatch => {
        return PostData('/make-payment', data)
        .then(response => {

            if(response.data.status === true) {
                dispatch(AppNotification(response.data.status, response.data.message, 'success'));
            }

            return response.data;
        })
        .catch(error => {
            return error.data;
        })
    }
}