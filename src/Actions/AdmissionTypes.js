import { PostData } from '../Services/PostData';
import { AppNotification, AppNotificationLoading } from './AppNotification';

export const SAVE_ADMISSION_TYPE = 'SAVE_ADMISSION_TYPE';
export const SUCCESS_ADMISSION_TYPE = 'SUCCESS_ADMISSION_TYPE';


export const successAdmissionTypes = data => ({

    type: SUCCESS_ADMISSION_TYPE,
    payload: data
});

export function updateAdmissionTypes(id, data) {
    return dispatch => {
        return PostData('/admission-type/' + id, data, 'PUT')
        .then(response => {

            if(response.data.status === true) {
                dispatch(AppNotification(response.data.status, response.data.message, 'success'))
            }

            return response.data.result;
        })
        .catch(error => {
            return error;
        })
    }
}

export function AdmissionTypesDetails(id) {
    return dispatch => {
        dispatch(AppNotificationLoading(true))

        return PostData('/admission-type/' + id, '', 'GET')
        .then(response => {
            dispatch(AppNotificationLoading(false))
            return response.data.result;
        })
        .catch(error => {
            return error;
        })
    }
}

export function deleteAdmissionTypes(id) {

    return dispatch => {
        
        return PostData('/admission-type/' + id, '', 'DELETE')
        .then(response => {

            if(response.data.status === true) {
                dispatch(AppNotification(response.data.status, response.data.message, 'success'))
            }

            return response.data.result;
        })
        .catch(error => {
            return error;
        })
    }
}

export function admissionTypes() {

    return dispatch => {
        dispatch(AppNotificationLoading(true))
        PostData('/admission-type', '', 'GET')
        .then(response => {
            dispatch(AppNotificationLoading(false))
            dispatch(successAdmissionTypes(response.data.result))
        })
    }
}

export function saveAdmissionTypes(data) {

    return dispatch => {
        
        return PostData('/admission-type', data)
        .then(response => {

            if(response.data.status === true) {
                dispatch(AppNotification(response.data.status, response.data.message, 'success'))
            }

            return response.data.result;
        })
        .catch(error => {
            return error;
        })
    }
}

