import { PostData } from '../Services/PostData';
// import { AppNotification, AppNotificationLoading } from './AppNotification';

export const SAVE_ORGANIZATION_SETTINGS = 'SAVE_ORGANIZATION_SETTINGS';


export const saveOrganizationSettings = data => ({
    type: SAVE_ORGANIZATION_SETTINGS,
    payload: data
});

export function getApplicationSettings() {
    return dispatch => {
        return PostData('/organization-settings', '', 'GET')
        .then(response => {

            if(response.data.status === true) {
                dispatch(saveOrganizationSettings(response.data.result));
            }

            return response.data;
        })
        .catch(error => {
            return error.data;
        })
    }
}