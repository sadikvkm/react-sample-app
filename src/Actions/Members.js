import { PostData } from '../Services/PostData';
import { AppNotification, AppNotificationLoading } from './AppNotification';

export const SAVE_CURRENT_CREATE_MEMBER = 'SAVE_CURRENT_CREATE_MEMBER';
export const SUCCESS_MEMBERS_LIST = 'SUCCESS_MEMBERS_LIST';


export const successMembersList = data => ({

    type: SUCCESS_MEMBERS_LIST,
    payload: data
});

export const saveCurrentCreateMember = data => ({

    type: SAVE_CURRENT_CREATE_MEMBER,
    payload: data
});

export function updateMemberIdProof(id, data) {
    return dispatch => {
        return PostData('/members/id-proof/' + id, data)
        .then(response => {

            return response.data;
        })
        .catch(error => {
            return error.data;
        })
    }
}

export function getMemberDetails(id) {
    return dispatch => {
        dispatch(AppNotificationLoading(true));

        return PostData('/members/' + id, '', 'GET')
        .then(response => {
            dispatch(AppNotificationLoading(false));

            if(response.data.status === true) {
                dispatch(saveCurrentCreateMember(response.data.result))
            }

            return response.data;
        })
        .catch(error => {
            return error.data;
        })
    }
}

export function saveMembers(data) {
    return dispatch => {
        return PostData('/members', data)
        .then(response => {

            if(response.data.status === true) {
                dispatch(AppNotification(response.data.status, response.data.message, 'success'));
                dispatch(saveCurrentCreateMember(response.data.result))
            }

            return response.data;
        })
        .catch(error => {
            return error.data;
        })
    }
}

export function membersList() {

    return dispatch => {
        dispatch(AppNotificationLoading(true));
        return PostData('/members', '', 'GET')
        .then(response => {
            dispatch(AppNotificationLoading(false));
            dispatch(successMembersList(response.data.result))
        })
        .catch(error => {
            return error.data;
        })
    }
}

