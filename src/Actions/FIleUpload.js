
import { PostData } from '../Services/PostData';
import { AppNotificationLoading } from './AppNotification';

export function fileUpload(file, type) {

    return dispatch => {
        dispatch(AppNotificationLoading(true))
        return PostData('/upload/' + type, file)
        .then(response => {

            let responseData = response.data;

            if(responseData.status === true) {
                dispatch(AppNotificationLoading(false))
                return responseData.result;
            }

            return null;
        })
        .catch(error => {
            return error;
        })
    }
}
