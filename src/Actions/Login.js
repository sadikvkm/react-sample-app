
import { PostData } from '../Services/PostData';
import { Auth } from '../Services/Auth';

export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';

export const successAuthentication = data => ({

    type: LOGIN_SUCCESS,
    payload: data
});
export const successLogout = data => ({

  type: LOGOUT_SUCCESS,
});

export function Authentication(data) {

  return dispatch => {
    
    return PostData('/login', data)
    .then(response => {

      let responseData = response.data.result;

      if(response.data.status === true) {
        Auth.attempt(responseData.user);
        Auth.setToken({
            token: responseData.access_token,
        });
      }
      dispatch(successAuthentication(data));
      return response;
    })
    .catch(error => {
      return error;
    })
  }
}

export function Logout(data) {

  return dispatch => {
    
    return PostData('/logout')
    .then(response => {
      
      if(response.data.status === true) {
        Auth.destroy();
      }
      dispatch(successLogout());
      return response;
    })
    .catch(error => {
      return error;
    })
  }
}

