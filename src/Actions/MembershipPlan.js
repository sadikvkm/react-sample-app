import { PostData } from '../Services/PostData';
import { AppNotification, AppNotificationLoading } from './AppNotification';

export const SUCCESS_MEMBERSHIP_PLAN = 'SUCCESS_MEMBERSHIP_PLAN';


export const successMembershipPlan = data => ({

    type: SUCCESS_MEMBERSHIP_PLAN,
    payload: data
});

export function updateMembershipPlan(id, data) {
    return dispatch => {
        return PostData('/membership-plan/' + id, data, 'PUT')
        .then(response => {

            if(response.data.status === true) {
                dispatch(AppNotification(response.data.status, response.data.message, 'success'))
            }

            return response.data.result;
        })
        .catch(error => {
            return error;
        })
    }
}

export function membershipPlanDetails(id) {
    return dispatch => {
        dispatch(AppNotificationLoading(true))

        return PostData('/membership-plan/' + id, '', 'GET')
        .then(response => {
            dispatch(AppNotificationLoading(false))
            return response.data.result;
        })
        .catch(error => {
            return error;
        })
    }
}

export function deleteMembershipPlan(id) {

    return dispatch => {
        
        return PostData('/membership-plan/' + id, '', 'DELETE')
        .then(response => {

            if(response.data.status === true) {
                dispatch(AppNotification(response.data.status, response.data.message, 'success'))
            }

            return response.data.result;
        })
        .catch(error => {
            return error;
        })
    }
}

export function membershipPlanList() {

    return dispatch => {
        dispatch(AppNotificationLoading(true))
        PostData('/membership-plan', '', 'GET')
        .then(response => {
            dispatch(AppNotificationLoading(false))
            dispatch(successMembershipPlan(response.data.result))
        })
    }
}

export function saveMembershipPlan(data) {

    return dispatch => {
        
        return PostData('/membership-plan', data)
        .then(response => {

            if(response.data.status === true) {
                dispatch(AppNotification(response.data.status, response.data.message, 'success'))
            }

            return response.data.result;
        })
        .catch(error => {
            return error;
        })
    }
}

