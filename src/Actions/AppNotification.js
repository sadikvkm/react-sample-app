export const NOTIFICATION = 'NOTIFICATION';
export const NOTIFICATION_LOADING = 'NOTIFICATION_LOADING';

export const showNotification = (data) => ({

    type: NOTIFICATION,
    payload: data
});

export const showNotificationLoading = (data) => ({

    type: NOTIFICATION_LOADING,
    payload: data
});

export function AppNotification(status, message = "", type = 'success') {

    return dispatch => {
        
        dispatch(showNotification({
            open: status,
            message: message,
            type: type
        }))  
    }
}

export function AppNotificationLoading(notify) {
    return dispatch => {
        
        dispatch(showNotificationLoading(notify))  
    }
}