import {

    LOGIN_SUCCESS

  } from '../Actions/Login';
  
  const initialState = {
    data: []
  };
  
  export default function LoginReducer(state = initialState, action) {

    switch(action.type) {

      case LOGIN_SUCCESS:

        return {
          ...state,
          data: action.payload
        };

      default:

        return state;
    }
  }