import {

    NOTIFICATION,
    NOTIFICATION_LOADING

  } from '../Actions/AppNotification';
  
  const initialState = {
    open: false,
    message: '',
    type: 'success',
    notify: false
  };
  
  export default function AppNotificationReducer(state = initialState, action) {

    switch(action.type) {

      case NOTIFICATION:

        return {
            ...state,
            open: action.payload.open,
            message: action.payload.message,
            type: action.payload.type,
            notify: action.payload.notify
        }

    case NOTIFICATION_LOADING:

        return {
            ...state,
            notify: action.payload
        }

      default:

        return state;
    }
  }