import {

    SUCCESS_MEMBERSHIP_PLAN

  } from '../Actions/MembershipPlan';
  
  const initialState = {
    list: [],
  };
  
  export default function MembershipPlanReducer(state = initialState, action) {

    switch(action.type) {

      case SUCCESS_MEMBERSHIP_PLAN:

        return {
            ...state,
            list: action.payload,
        }

      default:

        return state;
    }
  }