import {
    
    SAVE_CURRENT_CREATE_MEMBER,
    SUCCESS_MEMBERS_LIST

} from '../Actions/Members';
  
const initialState = {
    list: [],
    latestMember: ''
};
  
export default function MemberReducer(state = initialState, action) {

    switch(action.type) {

        case SUCCESS_MEMBERS_LIST:

        return {
            ...state,
            list: action.payload
        }

        case SAVE_CURRENT_CREATE_MEMBER:

            return {
                ...state,
                latestMember: action.payload
            }

        default:

            return state;
    }
}