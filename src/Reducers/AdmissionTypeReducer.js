import {

    SUCCESS_ADMISSION_TYPE

  } from '../Actions/AdmissionTypes';
  
  const initialState = {
    list: [],
  };
  
  export default function AdmissionTypeReducer(state = initialState, action) {

    switch(action.type) {

      case SUCCESS_ADMISSION_TYPE:

        return {
            ...state,
            list: action.payload,
        }

      default:

        return state;
    }
  }