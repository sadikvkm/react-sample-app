import {

    SAVE_ORGANIZATION_SETTINGS

} from '../Actions/ApplicationSettings';
  
const initialState = {
    admissionAmount: '',
};
  
export default function ApplicationSettingsReducer(state = initialState, action) {

    switch(action.type) {

      case SAVE_ORGANIZATION_SETTINGS:

        return {
            ...state,
            admissionAmount: action.payload.admission_amount,
        }

      default:

        return state;
    }
}