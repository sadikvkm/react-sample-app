import axios from 'axios';
import { API_URL } from '../Config/index';
import { Auth } from '../Services/Auth';

function PostData(type, postData, method) {
    
    return new Promise((resolve, reject) => {
        var headers = {
            'Content-Type': 'application/json',
            'Authorization': (Auth.getToken()) ? Auth.getToken().token : null
        }

        let fetch;

        if(method === "GET") {
            fetch = axios.get(API_URL + type, { headers: headers });
        }else if(method === "DELETE") {
            fetch = axios.delete(API_URL + type, { headers: headers });
        }else if(method === "PUT") {
            fetch = axios.put(API_URL + type, postData, { headers: headers });
        }else {
            fetch = axios.post(API_URL + type, postData, { headers: headers });
        }
        fetch.then(response => {
                resolve(response);
            })
            .catch(error => {

                if(error.response === undefined) {

                }else {

                    let status = error.response.status;

                    if(error.response.data === 'Invalid token.') {
                        console.log('error');
                        Auth.destroy();
                        // window.location.reload();
                    }

                    if(status === 401){

                    }else if(status === 400) {
                        // Auth.destroy();
                        // window.location.reload();
                    }
                    reject(error.response);
                }
            });
    });
}

export {
    PostData,
}
