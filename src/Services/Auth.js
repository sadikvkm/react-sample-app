export const Auth = {
    setToken(token) {
        localStorage.setItem('token', JSON.stringify(token));
        localStorage.setItem('expires', JSON.stringify(token.expires));
    },

    updateAuthDetails (data) {
        let user = JSON.parse(localStorage.getItem('user'));
        user[data.field] = data.data;
        localStorage.setItem('user', JSON.stringify(user));
    },

    getToken() {
        var token = JSON.parse(localStorage.getItem('token'));
        if(! token)
            return false;

        if(Date.now > parseInt(token.expires)) {
            return this.destroy();
        }else {

            return token;
        }
    },

    attempt(user) {
        localStorage.setItem('user', JSON.stringify(user));
    },

    Auth() {
        if(JSON.parse(localStorage.getItem('user'))){
            return JSON.parse(localStorage.getItem('user'));
        }
        return null;
    },

    Role() {
        if(localStorage.getItem('user'))
            return JSON.parse(localStorage.getItem('user')).role;
        else
            return null
    },

    setDeviceNumber(number) {
        if(! localStorage.getItem('deviceId')) {
            localStorage.setItem('deviceId', number);
        }
    },

    getDeviceNumber() {

        let number = localStorage.getItem('deviceId');
        if(number) {
            return number;
        }
        return '';
    },

    resetDeviceNumber(urlParams) {

        let number = localStorage.getItem('deviceId');
        if(number !== urlParams) {
            localStorage.removeItem('deviceId');
            this.setDeviceNumber(urlParams);
        }


    },

    async destroy () {
        await localStorage.removeItem('token');
        await localStorage.removeItem('user');
        await localStorage.removeItem('expires');
    }
};
